import java.awt.*;

import java.awt.event.*;

public class ItemDemo extends Frame implements ActionListener,AdjustmentListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6312307761135050438L;
	
	class WindowDemo extends WindowAdapter{
		@Override
		public void windowClosed(WindowEvent e) {
			
			super.windowClosed(e);
		}
	}
	Button b ;
	Label l;
	Scrollbar sbr;
	Scrollbar sbb;Scrollbar sbg;
	int count=0;
	public ItemDemo() {
		super(" Demo App");
		b= new Button("Count");
		b.setBounds(20, 40, 10, 5);
		b.addActionListener(this);
		sbr = new Scrollbar(Scrollbar.VERTICAL, 10, 0, 0, 100);
		sbg = new Scrollbar(Scrollbar.VERTICAL, 10, 20, 0, 100);
		sbb = new Scrollbar(Scrollbar.VERTICAL, 10, 20, 0, 100);
		sbr.setBounds(200, 300, 20, 50);
		sbg.setBounds(300, 300, 20, 50);
		sbb.setBounds(400, 300, 20, 50);
		sbr.addAdjustmentListener(this);
		sbg.addAdjustmentListener(this);
		sbb.addAdjustmentListener(this);
	


		l= new Label("count "+count );
		l.setAlignment(Label.CENTER);

		add(b);
		add(l);
		add(sbr);
		add(sbg);
		add(sbb);

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		count+=2;
		l.setName("Count is "+ count);

		if (count == 20) {
			b.setEnabled(false);
		}

	}

	public static void main(String[] args) {
		ItemDemo it = new ItemDemo();
		it.setLayout(new FlowLayout());
		it.setSize(500, 500);
		it.setVisible(true);

	}
	@Override
	public void adjustmentValueChanged(AdjustmentEvent e) {
		int red= sbr.getValue();
		int blue= sbb.getValue();
		int green= sbg.getValue();

		l.setBackground(new Color(red, green, blue));
	}

}
